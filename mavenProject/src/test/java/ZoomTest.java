public class ZoomTest {
    @BeforeSuite
    public void setUpAtSuiteLevel(){
        System.out.println("Running at suite level");
    }
    @BeforeClass
    public void setUpAtClassLevel(){
        System.out.println("Running before class");
    }
    @BeforeMethod
    public void beforeMethod(){
        System.out.println("Running before method");
    }
    @BeforeTest
    public void beforeTest(){
        System.out.println("Running Before Test");

    }
    @Test
    public void simpleTest(){
        System.out.println("Simple Test");

    }
    @AfterClass
    public void ClassLevel(){
        System.out.println("Running after Class");
    }
    @AfterSuite
    public void SuiteLevel(){
        System.out.println("Running after suite level");
    }
    @AfterMethod
    public void AfterMethod(){
        System.out.println("Running after method");
    }

    @AfterTest
    public void AfterTest(){
        System.out.println("running after test");
    }
    @Test
    public void Test(){
        System.out.println("test");
    }


}


